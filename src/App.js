import React, { Component } from "react";
import { Route, Switch } from "react-router";
import jwtDecode from "jwt-decode";

import CartPage from "./components/pages/CartPage";
import IndexPage from "./components/pages/IndexPage";
import SignupPage from "./components/pages/SignupPage";
import LoginPage from "./components/pages/LoginPage";
import Page404 from "./components/pages/Page404";
import Navigation from "./components/layouts/Navigation";
import setAuthHeaders from "./setAuthHeaders";
import api from "./api";

class App extends Component {
  state = {
    userId: null,
    authorized: false
  };

  componentWillMount() {
    if (localStorage.PharmacyToken) {
      setAuthHeaders(localStorage.PharmacyToken);
      const payload = jwtDecode(localStorage.PharmacyToken);
      this.setState({
        authorized: true,
        errors: "",
        userId: payload.user_id
      });
    } else {
      this.setState({ authorized: false });
    }
  }

  signUp = credentials =>
    api.auth
      .signup(credentials)
      .then(res => this.login(credentials.username, credentials.password));

  logout = () => {
    this.setState({ authorized: false, userId: null });
    setAuthHeaders();
    localStorage.removeItem("PharmacyToken");
  };

  login = (login, password) =>
    api.auth
      .login({ username: login, password })
      .then(res => {
        const payload = jwtDecode(res.access);
        localStorage.PharmacyToken = res.access;
        setAuthHeaders(res.access);
        this.setState({
          authorized: true,
          errors: "",
          userId: payload.user_id
        });
        this.props.history.push("/");
      })
      .catch(err =>
        this.setState({ errors: "Авторизационные данные не верны" })
      );

  render() {
    const { authorized, userId } = this.state;
    return (
      <>
        <Navigation authorized={authorized} logout={this.logout} />
        <Switch>
          <Route
            path="/"
            exact
            render={() => <IndexPage authorized={authorized} userId={userId} />}
          />
          <Route
            path="/cart"
            exact
            render={() =>
              authorized ? <CartPage userId={userId} /> : <Page404 />
            }
          />
          <Route
            path="/login"
            render={() => <LoginPage login={this.login} />}
          />
          <Route
            path="/signup"
            render={() => <SignupPage signup={this.signUp} />}
          />

          <Route component={Page404} />
        </Switch>
      </>
    );
  }
}

export default App;
