import axios from "axios";

const base = "http://127.0.0.1:8000/api";

export default {
  auth: {
    login: credentials =>
      axios.post(`${base}/token/`, credentials).then(res => res.data),
    signup: credentials => {
      console.log(credentials);
      return axios
        .post(`${base}/signup/`, { ...credentials })
        .then(response => response.data);
    }
  },
  medication: {
    getAll: () =>
      axios.get(`${base}/pharmacy/medication/`).then(res => res.data)
  },
  order: {
    get: userId =>
      axios.get(`${base}/pharmacy/order/${userId}/`).then(res => res.data),
    change: (userId, data) =>
      axios
        .post(`${base}/pharmacy/order/${userId}/`, data)
        .then(res => res.data)
  }
};
