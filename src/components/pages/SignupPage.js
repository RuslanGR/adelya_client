import React, { Component } from "react";

class LoginForm extends Component {
  state = {
    data: {
      username: "",
      password: "",
      password2: "",
      email: ""
    },
    errors: {},
    loading: false
  };

  onChange = e =>
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });

  onSubmit = e => {
    e.preventDefault();
    const errors = this.validate(this.state.data);
    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      delete this.state.data.password2;
      this.props.signup(this.state.data);
    }
  };

  validate = data => {
    const errors = {};
    if (!data.password) errors.password = "This field is required!";
    if (!data.password2) errors.password2 = "This field is required!";
    if (data.password !== data.password2)
      errors.password = "Passwords must be similar.";
    if (!data.username) errors.username = "Username is required!";
    return errors;
  };

  render() {
    const { errors } = this.state;
    return (
      <div className="container">
        <div className="row">
          <div className="offset-4 col-4">
            <h2 className="py-4">Зарегистрироваться</h2>
          </div>
        </div>
        <div className="row">
          <div className="offset-4 col-4">
            <form method="post" onSubmit={this.onSubmit} className="py-4">
              <div className="form-group">
                <label htmlFor="username">Username</label>
                <input
                  onChange={this.onChange}
                  type="text"
                  name="username"
                  className="form-control"
                  value={this.state.username}
                />
                {errors.username && (
                  <span style={{ color: "red" }}>{errors.username}</span>
                )}
              </div>
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <input
                  onChange={this.onChange}
                  type="text"
                  name="email"
                  className="form-control"
                  value={this.state.email}
                />
              </div>
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <input
                  onChange={this.onChange}
                  type="password"
                  name="password"
                  className="form-control"
                  value={this.state.password}
                />
                {errors.password && (
                  <span style={{ color: "red" }}>{errors.password}</span>
                )}
              </div>
              <div className="form-group">
                <label htmlFor="password2">Confirm password</label>
                <input
                  onChange={this.onChange}
                  type="password"
                  name="password2"
                  className="form-control"
                  value={this.state.password2}
                />
                {errors.password2 && (
                  <span style={{ color: "red" }}>{errors.password2}</span>
                )}
              </div>
              <button className="btn btn-dark">Sign up!</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default LoginForm;
