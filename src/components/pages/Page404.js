import React from "react";

const Page404 = () => (
  <div className="container">
    <h2 className="py-4">404</h2>
  </div>
);

export default Page404;
