import React from "react";
import api from "../../api";

class IndexPage extends React.Component {
  state = {
    search: "",
    medications: []
  };

  componentDidMount() {
    api.medication.getAll().then(res => this.setState({ medications: res }));
  }

  onChanhe = e => {
    this.setState({ search: e.target.value });
  };

  addToCart(id) {
    api.order.change(this.props.userId, { id, type: "ADD" });
  }

  render() {
    const { addToCart, authorized } = this.props;
    const { medications, search } = this.state;
    return (
      <div className="container">
        <h2 className="py-4">Главная страница</h2>
        <div className="row">
          <input
            type="text"
            className="form-control col-4"
            onChange={this.onChanhe}
            placeholder="Поиск"
          />
        </div>
        <div className="row pt-2">
          <table className="table pt-3">
            <thead className="thead-dark">
              <tr>
                <th scope="col">id</th>
                <th scope="col">Название</th>
                <th scope="col">Описание</th>
                <th scope="col">Цена</th>
                <th scope="col" />
              </tr>
            </thead>
            <tbody>
              {medications && !search
                ? medications.map(item => (
                    <tr key={item.id}>
                      <th scope="row">{item.id}</th>
                      <td>{item.name}</td>
                      <td>{item.description}</td>
                      <td>{item.price}р</td>
                      <td>
                        {authorized && (
                          <button
                            onClick={() => this.addToCart(item.id)}
                            className="btn btn-dark"
                          >
                            В корзину!
                          </button>
                        )}
                      </td>
                    </tr>
                  ))
                : medications
                    .filter(item => item.name.startsWith(search))
                    .map(item => (
                      <tr key={item.id}>
                        <th scope="row">{item.id}</th>
                        <td>{item.name}</td>
                        <td>{item.description}</td>
                        <td>{item.price}р</td>
                        <td>
                          {authorized && (
                            <button
                              onClick={() => addToCart(item.id)}
                              className="btn btn-dark"
                            >
                              В корзину!
                            </button>
                          )}
                        </td>
                      </tr>
                    ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default IndexPage;
