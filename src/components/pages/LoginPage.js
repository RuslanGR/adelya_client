import React, { Component } from "react";

class LoginPage extends Component {
  state = {
    login: "",
    password: "",
    errors: {}
  };

  submit = event => {
    event.preventDefault();
    const { login, password } = this.state;
    const errors = this.validate(this.state);
    this.setState({ errors });
    if (Object.keys(errors).length === 0) this.props.login(login, password);
  };

  validate = data => {
    const errors = {};
    if (!data.login) errors.login = "Login is required!";
    if (!data.password) errors.password = "Password is required!";
    return errors;
  };

  onChange = e => this.setState({ [e.target.name]: e.target.value });

  render() {
    const { login, password, errors } = this.state;

    return (
      <div className="container py-4">
        <div className="row">
          <div className="col-4 offset-4">
            <h2 className="py-4">Войти</h2>
          </div>
        </div>
        <div className="row">
          <div className="col-md-4 offset-md-4">
            {this.props.errors && (
              <h6 style={{ color: "red" }}>{this.props.errors}</h6>
            )}
            <form method="POST" onSubmit={this.submit}>
              <div className="form-group">
                <label htmlFor="">Логин</label>
                <input
                  tabIndex="0"
                  type="text"
                  className="form-control"
                  value={login}
                  onChange={this.onChange}
                  name="login"
                />
                {errors.login && (
                  <span style={{ color: "red" }}>{errors.login}</span>
                )}
              </div>
              <div className="form-group">
                <label htmlFor="">Пароль</label>
                <input
                  type="password"
                  className="form-control"
                  value={password}
                  onChange={this.onChange}
                  name="password"
                />
                {errors.password && (
                  <span style={{ color: "red" }}>{errors.password}</span>
                )}
              </div>
              <button className="btn btn-dark">Войти</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default LoginPage;
