import React from "react";
import api from "../../api";

class CartPage extends React.Component {
  state = {
    finalPrice: 0,
    medications: []
  };

  cartAdd = id => {
    api.order
      .change(this.props.userId, { id, type: "ADD" })
      .then(() => this.getMedications());
    this.getFinalPrice();
  };

  cartDelete = id => {
    api.order
      .change(this.props.userId, { id, type: "DELETE" })
      .then(() => this.getMedications());
    this.getFinalPrice();
  };

  getMedications = () => {
    api.order
      .get(this.props.userId)
      .then(res =>
        this.setState({ medications: res }, () => this.getFinalPrice())
      );
  };

  getFinalPrice = () => {
    const { medications } = this.state;
    this.setState({ finalPrice: 0 });
    medications.map(item => {
      this.setState(state => {
        return {
          finalPrice: state.finalPrice + item.count * item.medication.price
        };
      });
    });
  };

  componentDidMount() {
    this.getMedications();
  }

  render() {
    const { medications, finalPrice } = this.state;
    return (
      <div className="container">
        <div className="row">
          <h2 className="py-4">Корзина</h2>
        </div>
        <div className="row">
          <table className="table pt-3">
            <thead className="thead-dark">
              <tr>
                <th scope="col">id</th>
                <th scope="col">Название</th>
                <th scope="col">Описание</th>
                <th scope="col">Количество</th>
                <th scope="col">Цена</th>
                <th scope="col" />
              </tr>
            </thead>
            <tbody>
              {medications &&
                medications.map(item => (
                  <tr key={item.id}>
                    <th scope="row">{item.medication.id}</th>
                    <td>{item.medication.name}</td>
                    <td>{item.medication.description}</td>
                    <td>{item.count}</td>
                    <td>{item.medication.price}р</td>
                    <td>
                      <div className="btn-group">
                        <button
                          className="btn btn-dark"
                          onClick={() => this.cartAdd(item.medication.id)}
                        >
                          Добавить
                        </button>
                        <button
                          className="btn btn-dark"
                          onClick={() => this.cartDelete(item.medication.id)}
                        >
                          Удалить
                        </button>
                      </div>
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
        </div>
        <div className="row">
          <h4>Общая стоимость: {finalPrice}р</h4>
        </div>
      </div>
    );
  }
}

export default CartPage;
